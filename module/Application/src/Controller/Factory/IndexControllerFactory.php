<?php
namespace Application\Controller\Factory;

use Application\Model\MailchimpMembersTable;
use Application\Model\MailchimpMembers;
use Application\Controller\IndexController;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Db\TableGateway\TableGateway;

// Factory class
class IndexControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $controller, array $options = null) {
        $dbAdapter = $container->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new MailchimpMembers());
        $tableGateway = new TableGateway('mailchimp_members', $dbAdapter, null, $resultSetPrototype);
        return new $controller(new MailchimpMembersTable($tableGateway, $dbAdapter), $container->get('configuration')['mailchimp']);
    }
}