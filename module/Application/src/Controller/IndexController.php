<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Http\PhpEnvironment\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Mailchimp\Mailchimp;
use Application\Model\MailchimpMembersTable;
use Application\Model\MailchimpMembers;

class IndexController extends AbstractActionController
{
    private $db ;
    public $mailchipConfig;

    public function __construct(MailchimpMembersTable $db, array $mailchipConfig = [])
    {
        $this->db = $db;
        $this->mailchipConfig = $mailchipConfig;
    }

    /**
     * view main page
     * 
     * @return ViewModel
     * @author Sergiy Khomenko
     */
    public function indexAction()
    {
        $sync = $this->getRequest()->getQuery('sync');
        $members = $this->db->fetchAll();
        return new ViewModel(['members' => $members, 'sync' => $sync]);
    }

    /**
     * syncs mailchiml members into local db
     * 
     * @return bool|\Zend\Http\Response
     * @author Sergiy Khomenko
     */
    public function syncAction() 
    {
        $mailchimp = new Mailchimp($this->mailchipConfig);
        $mailchimpMembers = $mailchimp->getListMembers();
        if (!$mailchimp->isResponseSuccess()) {
            /* @todo add errors into view */
            echo $mailchimp->getErrors();
            return false;
        }
        $entity = new MailchimpMembers();
        foreach ($mailchimpMembers as $member) {
            $data = $entity->exchangeArray([
                'name' => $member['merge_fields']['FNAME'],
                'email' => $member['email_address'],
                'mailchimp_id' => $member['id']
            ]);
            $this->db->syncMembers($data);
        }
        return $this->redirect()->toRoute('home', ['action' => 'index'], ['query' => ['sync' => 'ok']]);
    }
}
