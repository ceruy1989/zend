<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\Model\MailchimpMembers;
use Application\Model\MailchimpMembersTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    const VERSION = '3.0.2dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                '\Application\Model\MailchimpMembersTable' =>  function($sm) {
                    $tableGateway = $sm->get('MailchimpMembersTableGateway');
                    $table = new MailchimpMembersTable($tableGateway);
                    return $table;
                },
                'MailchimpMembersTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MailchimpMembers());
                    return new TableGateway('mailchimp_members', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }

    public function getControllerConfig() {
        return [
            'factories' => [
                Controller\IndexController::class => function($container) {
                    return new Controller\IndexController(
                        $container->get(Model\MailchimpMembersTable::class)
                    );
                }
            ],
        ];
    }
}
