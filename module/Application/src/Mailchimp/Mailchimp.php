<?php
namespace Application\Mailchimp;

use Zend\Http\Client;
use Zend\Http\Client\Adapter\Curl;
use Zend\Json\Json;
use Zend\Http\Request;

class Mailchimp
{
    public $apiKey;
    public $listId;
    public $url;
    protected $response;
    protected $ResponseErrors;

    function __construct(array $config = []) 
    {
        $this->setApiKey(isset($config['api_key']) ? $config['api_key'] : null);
        $this->setListId(isset($config['list_id']) ? $config['list_id'] : null);
        $this->setUrl(isset($config['url']) ? $config['url'] : null);
    }

    /**
     * sets api key
     *
     * @param  $apiKey mailchimp user api key
     * @return void
     * @author Sergiy Khomenko
     */
    public function setApiKey($apiKey) 
    {
        $this->apiKey = $apiKey;
    }

    /**
     * sets list id
     *
     * @param  $listId mailchimp user list id
     * @return void
     * @author Sergiy Khomenko
     */
    public function setListId($listId) 
    {
        $this->listId = $listId;
    }

    /**
     * gets mailchimp api key
     *
     * @return null|string
     * @author Sergiy Khomenko
     */
    public function getApiKey() 
    {
        return $this->apiKey;
    }

    /**
     * gets mailchimp list id
     *
     * @return null|integer
     * @author Sergiy Khomenko
     */
    public function getListId() 
    {
        return $this->listId;
    }

    /**
     * sets mailchimp $url
     *
     * @param null|string mailchimp $url
     * @author Sergiy Khomenko
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * gets mailchimp url
     *
     * @return null|string
     * @author Sergiy Khomenko
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * gets list members from mailchimp by api
     *
     * @return mixed members
     * @author Sergiy Khomenko
     */
    public function getListMembers() 
    {
        $client = new Client();
        $dataCenter = substr($this->getApiKey(),strpos($this->getApiKey(),'-')+1);
        $url = 'https://' . $dataCenter . '.' . $this->getUrl() . 'search-members/?query=&' . 'list_id=' . $this->getListId();
        $client->setOptions(['adapter' => Curl::class]);
        $client->getAdapter()->setCurlOption(CURLOPT_USERPWD, 'user' . ':' . $this->getApiKey());
        $client->setMethod(Request::METHOD_GET);
        $client->setUri($url);
        $this->response = $client->send();
        return Json::decode($this->response->getBody(), true)['full_search']['members'];
    }

    /**
     * checks is response success
     *
     * @return bool
     * @author Sergiy Khomenko
     */
    public function isResponseSuccess() 
    {
        if ($this->response->isSuccess()) {
            return true;
        } 
        return false;
    }

    /**
     * gets response errors
     *
     * @return string
     * @author Sergiy Khomenko
     */
    public function getErrors() 
    {
        if ($this->response) {
            return Json::decode($this->response->getBody())->detail;
        } 
        return '';
    }
}