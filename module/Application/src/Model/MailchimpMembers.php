<?php
namespace Application\Model;

use Zend\Cache\Exception\InvalidArgumentException;

class MailchimpMembers
{
    public $id;
    public $name;
    public $email;
    public $mailchimp_id;

    /**
     * Exchange the current array with another array or object.
     *
     * @param  array|object $input
     * @return array        Returns the old array
     * @author Sergiy Khomenko
     *
     */
    public function exchangeArray(array $data = [])
    {
        if (!is_array($data)) {
            throw new InvalidArgumentException(
                'Passed variable is not an array or object, using empty array instead'
            );
        }
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->name = (isset($data['name'])) ? $data['name'] : null;
        $this->email = (isset($data['email'])) ? $data['email'] : null;
        $this->mailchimp_id = (isset($data['mailchimp_id'])) ? $data['mailchimp_id'] : null;
        return $this;
    }
}