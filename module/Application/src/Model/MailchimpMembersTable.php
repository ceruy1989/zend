<?php
namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;

class MailchimpMembersTable
{
    private $tableGateway;
    private $db;

    public function __construct(TableGateway $tableGateway, Adapter $db = null)
    {
        $this->tableGateway = $tableGateway;
        $this->db = $db;
    }

    /**
     * gets all members from local db
     *
     * @return \Zend\Db\ResultSet\ResultSet
     * @author Sergiy Khomenko
     */
    public function fetchAll()
    {
        $members = $this->tableGateway->select();
        return $members;
    }

    /**
     * syncs members into local db
     *
     * @param  MailchimpMembers|null $members
     * @return void
     * @author Sergiy Khomenko
     */
    public function syncMembers(MailchimpMembers $members = null)
    {
        $res = $this->db->query(
            "INSERT INTO {$this->tableGateway->getTable()} (name, email, mailchimp_id) 
            VALUES (:name , :email, :mailchimp_id) 
            ON DUPLICATE KEY UPDATE email = :email, name = :name"
        );
        $res->execute([
            'name' => $members->name,
            'email' => $members->email,
            'mailchimp_id' => $members->mailchimp_id
        ]);
    }
}